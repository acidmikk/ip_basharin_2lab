package ru.ulstu.is.sbapp.speaker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.ulstu.is.sbapp.speaker.service.SpeakerService;

@RestController
public class SpeakerController {
    private final SpeakerService speakerService;

    public SpeakerController(SpeakerService speakerService) {
        this.speakerService = speakerService;
    }

    @GetMapping("/")
    public String hello(@RequestParam(value = "num1", defaultValue = "2") Integer num1,
                        @RequestParam(value = "num2", defaultValue = "3") Integer num2) {
        return String.valueOf(speakerService.say(num1, num2));
    }
}
