package ru.ulstu.is.sbapp.speaker.domain;

public interface Speaker {
    double say(Integer num);
}
