package ru.ulstu.is.sbapp.speaker.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Speaker3 implements Speaker {
    private final Logger log = LoggerFactory.getLogger(Speaker3.class);

    @Override
    public double say(Integer num1) {
        return Math.pow(num1, 3);
    }

    public void init() {
        log.info("Speaker3.init()");
    }

    public void destroy() {
        log.info("Speaker3.destroy()");
    }
}
