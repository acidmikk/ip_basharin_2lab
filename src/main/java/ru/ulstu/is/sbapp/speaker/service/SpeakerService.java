package ru.ulstu.is.sbapp.speaker.service;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.ulstu.is.sbapp.speaker.domain.Speaker;

@Service
public class SpeakerService {
    private final ApplicationContext applicationContext;

    public SpeakerService(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public String say(Integer num1, Integer num2) {
        final Speaker speaker = (Speaker) applicationContext.getBean(String.valueOf(num2));
        return String.format("%s", speaker.say(num1));
    }
}
