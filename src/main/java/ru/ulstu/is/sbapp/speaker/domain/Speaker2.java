package ru.ulstu.is.sbapp.speaker.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component(value = "2")
public class Speaker2 implements Speaker {
    private final Logger log = LoggerFactory.getLogger(Speaker2.class);

    @Override
    public double say(Integer num1) {
        return Math.pow(num1, 2);
    }

    @PostConstruct
    public void init() {
        log.info("Speaker2.init()");
    }

    @PreDestroy
    public void destroy() {
        log.info("Speaker2.destroy()");
    }
}
