package ru.ulstu.is.sbapp.speaker.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.ulstu.is.sbapp.speaker.domain.Speaker;
import ru.ulstu.is.sbapp.speaker.domain.Speaker5;
import ru.ulstu.is.sbapp.speaker.domain.Speaker3;

@Configuration
public class SpeakerConfiguration {
    private final Logger log = LoggerFactory.getLogger(SpeakerConfiguration.class);

    @Bean(value = "3", initMethod = "init", destroyMethod = "destroy")
    public Speaker3 createRusSpeaker() {
        log.info("Call create3Speaker()");
        return new Speaker3();
    }

    @Bean(value = "5")
    public Speaker createEngSpeaker() {
        log.info("Call create5Speaker()");
        return new Speaker5();
    }
}
