package ru.ulstu.is.sbapp.speaker.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Speaker5 implements Speaker, InitializingBean, DisposableBean {
    private final Logger log = LoggerFactory.getLogger(Speaker5.class);

    @Override
    public double say(Integer num1) {
        return Math.pow(num1, 5);
    }

    @Override
    public void afterPropertiesSet() {
        log.info("Speaker5.afterPropertiesSet()");
    }

    @Override
    public void destroy() {
        log.info("Speaker5.destroy()");

    }
}
